import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function courseReducer(state = initialState.courses, action){
    switch(action.type){
        case types.CREATE_COURSE_SUCCESS:
            return [...state, //ex spread operator ..., just as thou we have typed all the arry elements  
                Object.assign({}, action.course)
            ]; // Or we could have just used concat(), remember we CANNOT use push() as push will mutate the array

        case types.UPDATE_COURSE_SUCCESS:
            return [
                ...state.filter(course => course.id !== action.course.id ),
                Object.assign({}, action.course)
            ];

        case types.LOAD_COURSES_SUCCESS:
            return action.courses;
            
        default:
            return state;
    }
}