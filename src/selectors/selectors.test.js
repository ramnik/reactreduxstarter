import expect from 'expect';
import { authorsFormattedForDropdown } from './selectors';

describe('Author Selectors', () => {
    describe('authorsFormattedForDropdown', () => {
        it('should return author data formatted for use in a dropdown', () => {
            const authors = [
                {id: 'abcd', firstName: 'John', lastName: 'Smith'},
                {id: 'xyz', firstName: 'Joe', lastName: 'Bloggs'}
            ];

            const expected = [
                {value: 'abcd', text: 'John Smith'},
                {value: 'xyz', text: 'Joe Bloggs'}
            ];

            expect(authorsFormattedForDropdown(authors)).toEqual(expected);
        });
    });
});